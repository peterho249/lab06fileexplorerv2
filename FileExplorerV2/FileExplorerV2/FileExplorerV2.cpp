﻿// FileExplorerV2.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "FileExplorerV2.h"
#include <windowsx.h>
#include <WinUser.h>
#include <CommCtrl.h>
#include <Shlwapi.h>
#include <shellapi.h>
#include <ShObjIdl.h>
#include <ShlObj.h>

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "Comctl32.lib")
#pragma comment(lib, "User32.lib")
#pragma comment(lib, "Shlwapi.lib")
#pragma comment(lib, "Shell32.lib")
#define MAX_LOADSTRING 100
#define BUFFER_SIZE 10240

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND listViewHandle;
HWND treeViewHandle;
HWND statusBarHandle;
int width, height;
bool sizingFLag = FALSE;


// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hwnd);
void GetSizeFromIni(int &width, int &height);
void WriteSizeToIni(int width, int height);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
void InitFolderColumn(HWND hwndListView);
void InitDriveColumn(HWND hwndListView);
void ClearListView(HWND hwndListView);
LPWSTR GetDateModified(const FILETIME &ftLastWrite);
WCHAR* GetSize(__int64 size);
void AddDirectoryInternal(IShellFolder *pShellFolder, LPITEMIDLIST pidlDirectory, HTREEITEM hParent);
HRESULT AddDirectory(HTREEITEM hParent, LPITEMIDLIST pidlDirectory);
HTREEITEM AddRoot();
HRESULT BindToIdl(LPCITEMIDLIST pidl, REFIID riid, void **ppv);
void LoadContentOfFolder(LPITEMIDLIST pidlDirectory);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_FILEEXPLORERV2, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FILEEXPLORERV2));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FILEEXPLORERV2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_FILEEXPLORERV2);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		//------------------------------
	case WM_NOTIFY:
	{
		int wmId = LOWORD(wParam);

		WCHAR* temp;
		switch (wmId)
		{
		case IDC_LISTVIEW:
		{
			LPNMHDR info = (LPNMHDR)lParam;
			if (NM_DBLCLK == info->code)
			{
				// Nhap doi vao doi tuong trong listview
				int index = ListView_GetNextItem(listViewHandle, -1, LVNI_FOCUSED);

				LVITEM item;
				item.mask = LVIF_PARAM;
				item.iSubItem = 0;
				item.iItem = index;

				ListView_GetItem(listViewHandle, &item);

				temp = (WCHAR*)item.lParam;

				ClearListView(listViewHandle);
				InitFolderColumn(listViewHandle);
				LoadContentOfFolder((LPITEMIDLIST)item.lParam);
			}
			else
			{
				if (info->code == NM_CLICK)
				{
					int index = ListView_GetNextItem(listViewHandle, -1, LVNI_FOCUSED);

					LVITEM item;
					item.mask = LVIF_PARAM;
					item.iSubItem = 3;
					item.iItem = index;

					WCHAR buffer[BUFFER_SIZE];
					ListView_GetItemText(listViewHandle, item.iItem, item.iSubItem, buffer, BUFFER_SIZE);
					SendMessage(statusBarHandle, SB_SETTEXT, 0, (LPARAM)buffer);
				}
			}
			break;
		}
		case IDC_TREEVIEW:
		{
			// A pointer to an NMHDR structure that contains
			// the notification code and additional information.
			// For some notification messages, 
			// this parameter points to a larger structure 
			// that has the NMHDR structure as its first member.

			LPNMTREEVIEW pnmtv = (LPNMTREEVIEW)lParam;
			NMHDR info = pnmtv->hdr;

			if (TVN_ITEMEXPANDING == info.code || TVN_SELCHANGED == info.code)
			{
				if (TreeView_GetChild(treeViewHandle, TreeView_GetSelection(treeViewHandle)) == NULL)
				{
					if (TreeView_GetRoot(treeViewHandle) == TreeView_GetSelection(treeViewHandle))
					{
					}
					else
					{
						ClearListView(listViewHandle);
						InitFolderColumn(listViewHandle);
						HTREEITEM hParent = TreeView_GetSelection(treeViewHandle);
						TVITEMEX tvItem;
						tvItem.mask = TVIF_PARAM;
						LPITEMIDLIST pidl = NULL;
						tvItem.hItem = hParent;
						tvItem.lParam = (LPARAM)pidl;
						TreeView_GetItem(treeViewHandle, &tvItem);
						AddDirectory(hParent, (LPITEMIDLIST)tvItem.lParam);
						LoadContentOfFolder((LPITEMIDLIST)tvItem.lParam);
					}
				}
				else
				{
					
						ClearListView(listViewHandle);
						InitFolderColumn(listViewHandle);
						HTREEITEM hParent = TreeView_GetSelection(treeViewHandle);
						TVITEMEX tvItem;
						tvItem.mask = TVIF_PARAM;
						LPITEMIDLIST pidl = NULL;
						tvItem.hItem = hParent;
						tvItem.lParam = (LPARAM)pidl;
						TreeView_GetItem(treeViewHandle, &tvItem);
						LoadContentOfFolder((LPITEMIDLIST)tvItem.lParam);
					
				}
				return FALSE;
			}
		}
		break;
		}
	}
		break;

		//------------------------------
	case WM_LBUTTONDOWN:
	{
		int xPos, yPos;

		xPos = (int)LOWORD(lParam);
		yPos = (int)HIWORD(lParam);

		RECT treeViewRect;
		GetClientRect(treeViewHandle, &treeViewRect);

		sizingFLag = (xPos > treeViewRect.right && xPos < treeViewRect.right + 3);

		if (sizingFLag)
		{
			SetCapture(hWnd);
			SetCursor(LoadCursor(NULL, IDC_SIZEWE));
		}

	}
		break;
	case WM_MOUSEMOVE:
	{
		int xPos, yPos;

		xPos = (int)LOWORD(lParam);
		yPos = (int)HIWORD(lParam);

		RECT treeViewRect;
		GetClientRect(treeViewHandle, &treeViewRect);

		RECT hWndRect;
		GetClientRect(hWnd, &hWndRect);

		if (wParam == MK_LBUTTON)
		{
			if (sizingFLag)
			{
				MoveWindow(treeViewHandle, 0, 0, xPos - 2, treeViewRect.bottom - treeViewRect.top, TRUE);
				MoveWindow(listViewHandle, xPos + 1, 0, hWndRect.right - (xPos + 1), treeViewRect.bottom - treeViewRect.top, TRUE);
			}
		}

		if (xPos > treeViewRect.right && xPos < treeViewRect.right + 3)
			SetCursor(LoadCursor(NULL, IDC_SIZEWE));
	}
		break;
	case WM_LBUTTONUP:
	{
		if (sizingFLag)
		{
			ReleaseCapture();
			sizingFLag = FALSE;
		}
	}
		break;
	case WM_CLOSE:
	{
		RECT clientRect;
		GetClientRect(hWnd, &clientRect);
		WriteSizeToIni(clientRect.right - clientRect.left + 16, clientRect.bottom - clientRect.top + 59);
	}
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	GetSizeFromIni(width, height);
	if (width != 0 && height != 0)
	{
		MoveWindow(hWnd, 0, 0, width, height, TRUE);
	}
	INITCOMMONCONTROLSEX icex;

	// Ensure that the common control DLL is loaded. 
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES | ICC_BAR_CLASSES;
	InitCommonControlsEx(&icex);

	// Get system font
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	RECT clientRect;
	GetClientRect(hWnd, &clientRect);

	treeViewHandle = CreateWindow(WC_TREEVIEW,
		L"",
		WS_VISIBLE | WS_CHILD | TVS_HASLINES | WS_VSCROLL | WS_HSCROLL |
		TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT,
		0,
		0,
		clientRect.right / 3 - 5,
		clientRect.bottom,
		hWnd,
		(HMENU)IDC_TREEVIEW,
		hInst,
		NULL);
	SendMessage(treeViewHandle, WM_SETFONT, (WPARAM)hFont, TRUE);

	listViewHandle = CreateWindow(WC_LISTVIEWW,
		L"",
		WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS | WS_VSCROLL | WS_HSCROLL,
		clientRect.right / 3, 0, clientRect.right * 2 / 3, clientRect.bottom,
		hWnd,
		(HMENU)IDC_LISTVIEW,
		hInst,
		NULL);
	SendMessage(listViewHandle, WM_SETFONT, (WPARAM)hFont, TRUE);

	statusBarHandle = CreateWindow(STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP,
		0, 0, 0, 0, hWnd, NULL, hInst, NULL);
	SendMessage(statusBarHandle, WM_SETFONT, (WPARAM)hFont, NULL);
	int statusBarSize[2] = { 100,-1 };
	SendMessage(statusBarHandle, SB_SETPARTS, 2, (LPARAM)&statusBarSize);

	AddRoot();

	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hwnd)
{
	PostQuitMessage(0);
}

WCHAR* GetSize(__int64 size)
{
	WCHAR* currencys[] = { L"B", L"KB", L"MB", L"GB", L"TB" };
	int currencyIndex = 0;

	__int64 right = 0;

	while (size >= 1024)
	{
		right = size % 1024;
		size /= 1024;
		currencyIndex++;
	}

	right /= 103;

	WCHAR *buffer = new WCHAR[15];
	wsprintf(buffer, L"%d.%d ", size, right);
	StrCat(buffer, currencys[currencyIndex]);
	return buffer;
}

void GetSizeFromIni(int &width, int &height)
{
	WCHAR buffer[BUFFER_SIZE];
	GetCurrentDirectory(BUFFER_SIZE, buffer);
	StrCat(buffer, L"\\config.ini");
	if (GetPrivateProfileString(NULL, NULL, NULL, NULL, NULL, buffer) == 0x2)
	{
		WritePrivateProfileString(L"FileExplorer", L"width", L"0", buffer);
		WritePrivateProfileString(L"FileExplorer", L"height", L"0", buffer);
		width = 0;
		height = 0;
	}
	else
	{
		WCHAR sizeBuffer[10];
		GetPrivateProfileString(L"FileExplorer", L"width", L"0", sizeBuffer, 10, buffer);
		width = wcstol(sizeBuffer, NULL, 10);
		GetPrivateProfileString(L"FIleExplorer", L"height", L"0", sizeBuffer, 10, buffer);
		height = wcstol(sizeBuffer, NULL, 10);
	}
}

void WriteSizeToIni(int width, int height)
{
	WCHAR buffer[BUFFER_SIZE];
	GetCurrentDirectory(BUFFER_SIZE, buffer);
	StrCat(buffer, L"\\config.ini");

	WCHAR sizeBuffer[10];
	wsprintf(sizeBuffer, L"%d", width);
	WritePrivateProfileString(L"FileExplorer", L"width", sizeBuffer, buffer);
	wsprintf(sizeBuffer, L"%d", height);
	WritePrivateProfileString(L"FileExplorer", L"height", sizeBuffer, buffer);
}

void OnSize(HWND hWnd, UINT state, int cx, int cy)
{
	RECT treeViewRect;
	GetClientRect(treeViewHandle, &treeViewRect);
	RECT statusBarRect;
	GetClientRect(statusBarHandle, &statusBarRect);

	MoveWindow(treeViewHandle, 0, 0, treeViewRect.right - treeViewRect.left, cy - (statusBarRect.bottom - statusBarRect.top), TRUE);
	MoveWindow(listViewHandle, treeViewRect.right + 3, 0, cx - (treeViewRect.right + 3), cy - (statusBarRect.bottom - statusBarRect.top), TRUE);
	MoveWindow(statusBarHandle, 0, 0, cx, 0, TRUE);
}

void InitFolderColumn(HWND hwndListView)
{
	LVCOLUMN lvColumn;

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 200;
	lvColumn.pszText = L"Name";
	ListView_InsertColumn(hwndListView, 0, &lvColumn);

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 150;
	lvColumn.pszText = L"Type";
	ListView_InsertColumn(hwndListView, 1, &lvColumn);

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.pszText = L"Date modified";
	ListView_InsertColumn(hwndListView, 2, &lvColumn);

	lvColumn.pszText = L"Size";
	ListView_InsertColumn(hwndListView, 3, &lvColumn);
}

void InitDriveColumn(HWND hwndListView)
{
	LVCOLUMN lvColumn;

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 200;
	lvColumn.pszText = L"Name";
	ListView_InsertColumn(hwndListView, 0, &lvColumn);

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 150;
	lvColumn.pszText = L"Capacity";
	ListView_InsertColumn(hwndListView, 1, &lvColumn);

	lvColumn.pszText = L"Free space";
	ListView_InsertColumn(hwndListView, 2, &lvColumn);

	lvColumn.pszText = L"Type";
	ListView_InsertColumn(hwndListView, 3, &lvColumn);
}

void ClearListView(HWND hwndListView)
{
	ListView_DeleteAllItems(hwndListView);

	//Source: https://stackoverflow.com/questions/33281164/how-to-get-the-number-of-columns-in-a-list-control
	HWND hWndHdr = (HWND)::SendMessage(hwndListView, LVM_GETHEADER, 0, 0);
	int count = (int)::SendMessage(hWndHdr, HDM_GETITEMCOUNT, 0, 0L);

	for (int i = 0; i < count; i++)
	{
		ListView_DeleteColumn(hwndListView, 0);
	}
}

LPWSTR GetDateModified(const FILETIME &ftLastWrite)
{

	//Chuyển đổi sang local time
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	TCHAR *buffer = new TCHAR[50];
	wsprintf(buffer, _T("%02d/%02d/%04d %02d:%02d"),
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		stLocal.wHour,
		stLocal.wMinute);

	return buffer;
}

void LoadContentOfFolder(LPITEMIDLIST pidlDirectory)
{
	IEnumIDList *pEnumIDList = NULL;
	LPITEMIDLIST rgelt = NULL;
	SHCONTF EnumFlags;
	HRESULT hr;
	IShellFolder *pShellFolder = NULL;

	hr = BindToIdl(pidlDirectory, IID_PPV_ARGS(&pShellFolder));

	EnumFlags = SHCONTF_FOLDERS | SHCONTF_NONFOLDERS;

	hr = pShellFolder->EnumObjects(NULL, EnumFlags, &pEnumIDList);

	if (SUCCEEDED(hr) && pEnumIDList != NULL)
	{
		int count = 0;
		while (pEnumIDList->Next(1, &rgelt, NULL) == S_OK)
		{
			ULONG Attributes = SFGAO_FOLDER | SFGAO_FILESYSTEM | SFGAO_STORAGE;

			hr = pShellFolder->GetAttributesOf(1, (LPCITEMIDLIST*)&rgelt, &Attributes);
			if (SUCCEEDED(hr))
			{
				if ((Attributes & SFGAO_FOLDER) && (Attributes & SFGAO_FILESYSTEM))
				{ 
					// Neu la thu muc
					LPITEMIDLIST pidlComplete = NULL;
					STRRET str;
					TCHAR ItemName[BUFFER_SIZE];

					hr = pShellFolder->GetDisplayNameOf(rgelt, SHGDN_NORMAL, &str);

					if (SUCCEEDED(hr))
					{
						StrRetToBuf(&str, rgelt, ItemName, BUFFER_SIZE);

						pidlComplete = ILCombine(pidlDirectory, rgelt);

						SHFILEINFO shfi;
						SHGetFileInfo((LPTSTR)pidlComplete, NULL, &shfi, sizeof(shfi), SHGFI_PIDL | SHGFI_SYSICONINDEX | 
							SHGFI_DISPLAYNAME | SHGFI_TYPENAME);

						LVITEM lvItem;
						lvItem.mask = LVIF_TEXT | LVIF_PARAM;

						lvItem.pszText = ItemName;
						lvItem.lParam = (LPARAM)pidlComplete;
						lvItem.iItem = count;
						lvItem.iSubItem = 0;
						ListView_InsertItem(listViewHandle, &lvItem);
						ListView_SetItemText(listViewHandle, count, 1, shfi.szTypeName);
						
					}
				}
				else
				{
					// neu la tap tin
					LPITEMIDLIST pidlComplete = NULL;

					if (SUCCEEDED(hr))
					{
						pidlComplete = ILCombine(pidlDirectory, rgelt);

						SHFILEINFO shfi;
						SHGetFileInfo((LPTSTR)pidlComplete, NULL, &shfi, sizeof(shfi), SHGFI_PIDL | SHGFI_SYSICONINDEX | 
							SHGFI_DISPLAYNAME | SHGFI_TYPENAME);

						LVITEM lvItem;
						lvItem.mask = LVIF_TEXT | LVIF_PARAM;

						lvItem.pszText = shfi.szDisplayName;
						lvItem.lParam = (LPARAM)pidlComplete;
						lvItem.iItem = count;
						lvItem.iSubItem = 0;
						ListView_InsertItem(listViewHandle, &lvItem);
						ListView_SetItemText(listViewHandle, count, 1, shfi.szTypeName);
						
						if (StrStr(shfi.szTypeName, L"Folder") == NULL && StrStr(shfi.szTypeName, L"folder") == NULL && StrStr(shfi.szTypeName, L"Shortcut") == NULL)
						{
							WIN32_FIND_DATA winFindData;
							SHGetDataFromIDList(pShellFolder, rgelt, SHGDFIL_FINDDATA, &winFindData, sizeof(winFindData));
							ListView_SetItemText(listViewHandle, count, 2, GetDateModified(winFindData.ftLastWriteTime));
							ListView_SetItemText(listViewHandle, count, 3, GetSize(winFindData.nFileSizeLow));
						}
					}
				}
			}

			count++;
		}
	}
}

// Source: https://github.com/derceg/explorerplusplus/blob/master/Explorer%2B%2B/MyTreeView/MyTreeView.cpp
// Following function are referenced from above source.
HTREEITEM AddRoot()
{
	IShellFolder *pDesktopFolder = NULL;
	LPITEMIDLIST pidl = NULL;
	SHFILEINFO shfi;
	HRESULT hr;
	TVINSERTSTRUCT tvis;
	TVITEMEX tvItem;
	HTREEITEM hDesktop = NULL;

	TreeView_DeleteAllItems(treeViewHandle);
	hr = SHGetFolderLocation(NULL, CSIDL_DESKTOP, NULL, 0, &pidl);

	if (SUCCEEDED(hr))
	{
		STRRET str;
		TCHAR ItemName[BUFFER_SIZE];
		SHGetFileInfo((LPTSTR)pidl, NULL, &shfi, sizeof(shfi), SHGFI_PIDL | SHGFI_SYSICONINDEX);
		SHGetDesktopFolder(&pDesktopFolder);
		pDesktopFolder->GetDisplayNameOf(pidl, SHGDN_NORMAL, &str);
		StrRetToBuf(&str, pidl, ItemName, BUFFER_SIZE);
		tvItem.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_CHILDREN | TVIF_PARAM;
		tvItem.pszText = ItemName;
		tvItem.iImage = shfi.iIcon;
		tvItem.cChildren = 1;
		tvItem.lParam = (LPARAM)pidl;

		tvis.hParent = NULL;
		tvis.hInsertAfter = TVI_ROOT;
		tvis.itemex = tvItem;

		hDesktop = TreeView_InsertItem(treeViewHandle, &tvis);

		if (hDesktop != NULL)
		{
			hr = SHGetDesktopFolder(&pDesktopFolder);
			if (SUCCEEDED(hr))
			{
				AddDirectoryInternal(pDesktopFolder, pidl, hDesktop);
				pDesktopFolder->Release();
			}
		}
	}
	return hDesktop;
}

HRESULT AddDirectory(HTREEITEM hParent, LPITEMIDLIST pidlDirectory)
{
	IShellFolder *pShellFolder = NULL;
	HRESULT hr;
	hr = BindToIdl(pidlDirectory, IID_PPV_ARGS(&pShellFolder));

	if (SUCCEEDED(hr))
	{
		AddDirectoryInternal(pShellFolder, pidlDirectory, hParent);
		pShellFolder->Release();
	}

	return hr;
}

void AddDirectoryInternal(IShellFolder *pShellFolder, LPITEMIDLIST pidlDirectory, HTREEITEM hParent)
{
	IEnumIDList *pEnumIDList = NULL;
	LPITEMIDLIST rgelt = NULL;
	SHCONTF EnumFlags;
	TVINSERTSTRUCT tvis;
	TVITEMEX tvItem;
	HRESULT hr;

	EnumFlags = SHCONTF_FOLDERS;

	hr = pShellFolder->EnumObjects(NULL, EnumFlags, &pEnumIDList);

	if (SUCCEEDED(hr) && pEnumIDList != NULL)
	{
		while (pEnumIDList->Next(1, &rgelt, NULL) == S_OK)
		{
			ULONG Attributes = SFGAO_FOLDER | SFGAO_FILESYSTEM;

			hr = pShellFolder->GetAttributesOf(1, (LPCITEMIDLIST*)&rgelt, &Attributes);
			if (SUCCEEDED(hr))
			{
				if (Attributes & SFGAO_FOLDER)
				{
					LPITEMIDLIST pidlComplete = NULL;
					STRRET str;
					TCHAR ItemName[BUFFER_SIZE];
					
					hr = pShellFolder->GetDisplayNameOf(rgelt, SHGDN_NORMAL, &str);

					if (SUCCEEDED(hr))
					{
						StrRetToBuf(&str, rgelt, ItemName, BUFFER_SIZE);

						pidlComplete = ILCombine(pidlDirectory, rgelt);

						SHFILEINFO shfi;
						SHGetFileInfo((LPTSTR)pidlComplete, NULL, &shfi, sizeof(shfi), SHGFI_PIDL | SHGFI_SYSICONINDEX);

						tvItem.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_CHILDREN | TVIF_PARAM;
						tvItem.pszText = ItemName;
						tvItem.iImage = shfi.iIcon;
						tvItem.cChildren = 1;
						tvItem.lParam = (LPARAM)pidlComplete;

						tvis.hParent = hParent;
						tvis.hInsertAfter = TVI_LAST;
						tvis.itemex = tvItem;

						TreeView_InsertItem(treeViewHandle, &tvis);
					}
				}
			}
		}
	}
}

HRESULT BindToIdl(LPCITEMIDLIST pidl, REFIID riid, void **ppv)
{

	IShellFolder *pDesktop = NULL;

	HRESULT hr = SHGetDesktopFolder(&pDesktop);

	if (SUCCEEDED(hr))
	{
		/* See http://blogs.msdn.com/b/oldnewthing/archive/2011/08/30/10202076.aspx. */

		if (pidl->mkid.cb)
		{
			hr = pDesktop->BindToObject(pidl, NULL, riid, ppv);
		}
		else
		{
			hr = pDesktop->QueryInterface(riid, ppv);
		}
		pDesktop->Release();
	}
	return hr;
}