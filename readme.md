# README #

## Information ##
* ID: 1512077
* Name: Ho Xuan Dung

## Configuration System ##
* OS: Windows 10 x64
* IDE: Visual Studio 2017

## Function ##
* Using Shell interface to get file and folder list.
* Resize list view and tree view.
* Restore the last window size from ini file.
* Show file size on status bar.

## Application Flow ##
### Main Flow ###
* Click on folder name on tree view, folders and files will be shown on list view.
* Double click on folder on list view, that folder will be opened.
* Double click on folder on tree view or press collapse button, subfolder will be shown below that folder.
* Move mouse cursor to the edge of tree view and drag, the list view and tree view will be resized.
* Click on a file, the size of that file will be shown on status bar.
* Resize the window, then close it. When the app is opened, it will show at last size.

### Addiction Flow ###
* Cannot double click a file to open.

## BitBucket Link ##
* https://peterho249@bitbucket.org/peterho249/lab06fileexplorerv2.git

## Youtube Link ##
* https://youtu.be/66NBrdWm4-s